//validação de entrada
/*var schema = {
    properties: {
      name: {
        pattern: /^[a-zA-Z\s\-]+$/,
        message: 'Name must be only letters, spaces, or dashes',
        required: true
      },
      password: {
        hidden: true
      }
    }
  };*/

var prompt = require('prompt');
var colors = require("colors/safe");
//
// Setting these properties customizes the prompt.
//
prompt.message = colors.rainbow("Question!");
prompt.start();

prompt.get(['username', 'email'], function(err, result){


    console.log('Command-line input received:');
    console.log(' username: ' + result.username);
    console.log(' email: ' + result.email);
});