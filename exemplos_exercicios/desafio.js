const faker = require('faker');
faker.locale = 'pt_BR';

/*var prompt = require('prompt');
prompt.start();*/

const Clients = [];
//const genders=[ 'F' , 'M' ];

for (let index = 0; index < 100; index++) {
    const name = faker.name.findName();
    const birth = faker.date.past(109, 2019);
        //gender:faker.random.arrayElement(genders),
    const genre = faker.random.boolean() == true ? 'M' : 'F';
    const lastpurchase = faker.date.past(10,2019);
    const countpurchase = faker.random.number(50);
  
    client = {
      name,
      birth,
      genre,
      lastpurchase,
      countpurchase,
    }
    Clients.push(client)
}


/*  1.  Desenvolva, utilizando filter , uma função que, dado um caractere de entrada, 
    retorne todos os registros de clientes cujo o nome inicia com o caractere.  */
function busca_letra1(x){

    let firstLetter = Clients.filter(value => (value.name[0]=='M'))
    return firstLetter;
}
    console.log('1.Busca registros com uma letra x')
    console.log(busca_letra1())
    console.log('========================================================')


/*  2.  Retorne o array de clientes*/
    console.log('2.Retorna array de clientes')
    console.log(Clients)
    console.log('========================================================')

/*  3.  Desenvolva uma função que, dado o caractere de entrada, retorna apenas um número com o total de registros encontrados.*/
function busca_caractere(x){

    let totalCaracteres = Clients
        .map(value => (value.name[0]=='M'))
        .reduce((acc, next) => acc += next);
        return totalCaracteres;

}
    console.log('3.Retorna o número do total de registros com o caractere de entrada')
    console.log(busca_caractere());
    console.log('========================================================')

/*  4.  Desenvolva uma função que retorne apenas os nomes dos clientes. */
function busca_clientes(name){

    let nameClients = Clients.map(value => value.name)
    return nameClients;
    
    }
    console.log('4.Retorna apenas nome dos clientes')
    console.log(busca_clientes());

/*  5. Desenvolva uma função que retorne apenas o primeiro nome dos clientes.   */
function busca_1nome(name){

    let firstName = Clients.map(value => value.name.split(" ")[0]);
    return firstName;

        
        /*let firstName = Clients.filter(value =>{ 
            if (value.name[0,3] == 'Sr.' || 'Dr.') {
                firstName.map(value => value.name.split(".")[0])
                return firstName;        
            } else {
                firstName.map(value => value.name.split(" ")[0])
                return firstName;
            } 
        }); */
        
}
    console.log('5.Retorna apenas o 1º nome dos clientes')
    console.log(busca_1nome());

/*  6.  Desenvolva uma função que retorne apenas o primeiro nome dos clientes cujo os nomes começam com o caractere de entrada da função.*/
function busca_nome_char(x){

    let firstLetter2 = Clients.filter(value => value.name[0]=='M')
    firstLetter2 = firstLetter2.map(value => value.name.split(" ")[0]);
    
    return firstLetter2;
}
    console.log('6. Retorna o 1º nome apenas com um caractere x')
    console.log(busca_nome_char())
    console.log('========================================================')

/*  7.  Desenvolva uma função que retorne todos os usuários que são maiores de idade.   */
/*function maiores() {   //ESSA NÃO FUNCIONOU
    const maiores18 = birth[0,3].filter(value => {
     idade = value - 2020
     if (idade>18){
        return maiores18;
        }
    });
}    
    console.log('7. Retorna usuários maiores de 18 anos')
    console.log(maiores())
    console.log('========================================================')*/

/*  8.  Desenvolva uma função que, dado um nome de entrada, retorna se o nome está contido na lista.*/
function contem_nome(entra_nome) {
    let nomeContido = Clients.map(value => value.name.split(" ")[0])
                            .includes(entra_nome)? (`O nome ${entra_nome} está contido`):(`O nome ${entra_nome} NÃO está contido`)
                            return nomeContido;
    
}
    console.log('8. Retorna se o nome de entrada está contido na lista')
    console.log(contem_nome('Margarida'))
    console.log('========================================================')

/*  9.  Implemente uma função que retorna o total de vendas realizadas somando as compras de todos os clientes.*/
function total_vendas() {
    let somaCompras = Clients.map(value => value.countpurchase)
                            .reduce((acc, next) => (acc += next));
                            return somaCompras;
              
}
console.log('9. Retorna o total de vendas realizadas somando as compras de todos clientes')
console.log(total_vendas())
console.log('========================================================')

/*  10. Implemente uma função que retorne os dados dos clientes que não compram há mais de 1 ano.*/

/*  11. Implemente uma função que retorne os dados dos clientes que já realizaram mais de 15 compras. */
function maisDe15Compras() {
    const maisDe15 = Clients.filter(value=>value.countpurchase>15)
                            return maisDe15
}
console.log('11. Clientes que já realizaram mais de 15 compras')
console.log(maisDe15Compras())
console.log('========================================================')


