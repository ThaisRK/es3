const clients = [
    {name: 'Thais', birth: 1993, genre: 'F', purchaseDaysAgo:1},
    {name: 'Chuck', birth: 1940, genre: 'M', purchaseDaysAgo:2},
    {name: 'Sansa', birth: 2002, genre: 'F', purchaseDaysAgo:70},
    {name: 'Dean', birth: 1982, genre: 'M', purchaseDaysAgo:7},
    {name: 'Ragnar', birth: 1960, genre: 'M', purchaseDaysAgo:25},

]

let ages = [17, 21, 16, 9, 22, 12, 43, 99, 44, 32];
console.log(ages);

//Altera todos valores em +1
/*for (let index = 0; index < ages.length; index++) {
    ages[index] = ages[index]+1;}*/

    //Adiciona uma função em cada elemento do array
        //value é cada item de ages
            //arrow function: o aray que está sendo retornando em ages.map 
                //vai para let ages, atualizando-a
                    //.MAP() MODIFICA TODOS OS ÍTENS DO ARRAY
ages = ages.map(value => value + 1);
console.log('ages: ', ages);
        //Acessar, printar valores no array sem alterar
ages.forEach(element => {
    console.log(element);
});

console.log('================================================')   

//Retorna todos os valore ímpares
        //.FILTER/() FILTRA TODOS ELEMENTOS DO ARRY:
            //value é cada item de ages
                //se retornar true: cai no filtro, false: não Cai no filtro
const impares = ages.filter(value => (value%2==1));
console.log('impares: ', impares);
console.log('================================================')   


//Altera todos valores ímpares para o próximo par
    //quando tem processamento além do return, aplica-se este dentro das chaves
                /*ages = ages.map(value => {
                    if(value%2==0){
                        return value;
                    }else{
                        return value+1;
                    }
                });
                console.log('ages: ', ages)*/
ages = ages.map(value => (value %2 ==1? value+1 : value));
console.log('ages impares para próxima par: ', ages);
console.log('================================================')   


//Cria um array com os valores menores que 20
const menores = ages.filter(value => value<20);
console.log('menores: ', menores)
console.log('================================================')   

//Criar um novo array com os valores menores que 17 e alterados em +1
const menores2 = ages
    .filter(value => value < 17)
    .map(value => value+1);
console.log('menores2: ', menores2)
console.log('================================================')   

//Retorna a soma de todos os valores menores que 20
const somaMenores20 = ages
                .filter(value => value < 20)
                //.REDUCE : reduz e tem um retrno único... 
                //reduz o array em um valor
                .reduce((acc, next) => acc += next);
console.log('Soma Menores que 20: ',somaMenores20);
                /*com valor inicial
                .reduce((acc, next) => {
                    return acc += next}, 1000);*/
console.log(clients)
console.log('================================================')                    

//Incrementa em 1 o purchaseDaysAgo de todos os objetos
//clona o clients dentro de newClients
let newClients = clients.map(v=>({...v}))
    newClients = newClients.map(value => {
    value.purchaseDaysAgo+=1;
    return value;
})
console.log('newClients: ', newClients)
console.log('================================================')   

//Verifica se um determinado número de entrada está contido no array ages
//verifica se () inclui ou não
console.log('ages.includes(19) :', ages.includes(18));
console.log('ages.find(18): ', ages.find(v => v == 18));
console.log('================================================')   

// Altera array para ficar só com o primeiro nome dos clientes
let nameClients = clients.map(value => value.name.split(" ")[0]);
console.log('clients: ', nameClients);
console.log('================================================')   

//Incrementa aidade dos clientas nas posições impares

//Retorna todas as pessoas de sexo masculino