"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Client = /*#__PURE__*/function () {
  function Client(name, cpf, birth) {
    _classCallCheck(this, Client);

    this.name = name;
    this.cpf = cpf;
    this.birth = birth;
    this.enable = false;
  }

  _createClass(Client, [{
    key: "setEnable",
    value: function setEnable(status) {
      this.enable = status;
    }
  }, {
    key: "getEnable",
    value: function getEnable() {
      return this.enable;
    }
  }]);

  return Client;
}();

var client = new Client('Thais', '035.363.610-05', '10/11/1993');
client.setEnable(true);
console.log(client);
