class Client {
  constructor(name, cpf, birth) {
    this.name = name;
    this.cpf = cpf;
    this.birth = birth;
    this.enable = false;
  }

  setEnable(status) {
    this.enable = status;
  }

  getEnable() {
    return this.enable;
  }
}

const client = new Client('Thais', '035.363.610-05', '10/11/1993')
client.setEnable(true);
console.log(client);