//converter funções para arrow function -- função anônima

function proc(age){
    if (age <18){
        return '/index';
    }else{
        return'/home';
    }
}

const proc2 = (age) => {
    if (age <18){
        return '/index';
    }else{
        return'/home';
    }
}

//------------------------------------------------------
//quando se tem um só retorno: traz ele para após a arrow
function sum(v1, v2){
    return v1+v2;
}

const sum2 = (v1, v2) => v1+v2;

//------------------------------------------------------
//quando se tem um único parâmetro(não precisa dos parênteses) | quando se tem um único retorno(traz ele para após o arrow)
function isPositive(v1){
    return v1>0;
}

const isPositive2 = v1 => v1>0;

//------------------------------------------------------
//para uma funcção rodar somente uma vez(forma de encapsulamento): cria-se função anônima, que deve ser envolvido entre parênteses
function init() {
    //bloco de código
    console.log('Helo!');
}
init();

(function () {
    //bloco de código
    console.log('Helo!');
}());



