class Client{
    constructor(name){
        this.name = name;
        this.purchaseDaysAgo = 0;
    }

    printNameFunction() {
        setInterval(function () {

            console.log(this.name);
            console.log(this.purchaseDaysAgo+=1);
        }, 1000);
    } //a função tem seu próprio this, por isso considera o name local

    printNameFunction2() {
        setInterval( () => {

            console.log(this.name);
            console.log(this.purchaseDaysAgo+=1);
        }, 1000);
    }
}

c = new Client("Fulano");
c.printNameFunction2();