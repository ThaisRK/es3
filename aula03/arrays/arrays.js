const clients = [
    {name: 'Thais', birth: 1993, genre: 'F', purchaseDaysAgo:1},
    {name: 'Chuck', birth: 1940, genre: 'M', purchaseDaysAg5:2},
    {name: 'Sansa', birth: 2002, genre: 'F', purchaseDaysAgo:70},
    {name: 'Dean', birth: 1982, genre: 'M', purchaseDaysAgo:7},
    {name: 'Ragnar', birth: 1960, genre: 'M', purchaseDaysAgo:25},

]

let ages = [17, 21, 16, 9, 22, 12, 43, 99, 44, 32];
console.log(ages);

//Altera todos valores em +1

/*for (let index = 0; index < ages.length; index++) {
    ages[index] = ages[index]+1;}*/

//(((Adiciona uma função em cada elemento do array)))
        //value é cada item de ages
            //arrow function: o aray que está sendo retornando em ages.map 
                //vai para let ages, atualizando-a
                    //.MAP() MODIFICA TODOS OS ÍTENS DO ARRAY
ages = ages.map(value => value + 1);
console.log('ages: ', ages);


//(((Retorna todos os valore ímpares)))
        //.FILTER/() FILTRA TODOS ELEMENTOS DO ARRY:
            //value é cada item de ages
                //se retornar true: cai no filtro, false: não Cai no filtro
const impares = ages.filter(value => (value%2==1));
console.log('impares: ', impares);


//(((Altera todos valores ímpares para o próximo par)))
    //quando tem processamento além do return, aplica-se este dentro das chaves
                /*ages = ages.map(value => {
                    if(value%2==0){
                        return value;
                    }else{
                        return value+1;
                    }
                });
                console.log('ages: ', ages)*/


//ages = ages.map(value => (value %2 ==1? value+1));



